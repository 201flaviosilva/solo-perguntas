# SoloPerguntas

## Description

Esta Aplicação tem como objetivo que cada vez que se tocar em um botão pré-defenido apareça uma pergunta aleatória, (nem sempre aparece uma pergunta, ás vezes tambem pode aparecer uma frase com sentido de humor, triste, ou filosófica), e com isso o utilizador poder responder livremente sobre a sua vida pessoal ou não responder sobre si ou até mesmo não responder, pois a perguntas não se relaçionam estre si. isto tudo sem ser nessecario preocupar com segurança, fazendo assim com que o utilizador ao responder ás questões de uma maquina talvez melhore de algo que lhe possa ter acontecido de mal!

O objetivo destas perguntas é tentar que o Utilizador tente se libertar e possa dizer o que sempre quiz dizer, como contar uma história. Algumas perguntas podem ser de resposta de apenas "Sim" ou "Não", mas o objetivo não é esse. O objetivo é que se responda de uma forma desenvolvida para obter o verdadeiro proveito da Aplicação.

## Preview

![Preview](https://bitbucket.org/201flaviosilva/solo-perguntas/downloads/Preview.gif)